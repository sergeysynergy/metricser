# Metrics collection and alerting service in Go

Run server to start collecting metrics:
```
cd cmd/server
go run .
```

Run agent to start populate metrics:
```
cd cmd/agent
go run .
```

Run all tests:
```
make test
```

# Service configuration

You can configure service using environment options or flags. Environment options have priority before the flags options.

### Server options
- -a (ADDRESS) - address to listen on, default `127.0.0.1:8080`
- -r (RESTORE) - restore metrics from file, default `true`
- -i (STORE_INTERVAL) - write metrics to the file interval, default `300s`
- -f (STORE_FILE) - file to store metrics, default `/tmp/devops-metrics-db.json`
- -d (DATABASE_DSN) - Postgres DSN 

### Agent options
- -a (ADDRESS) - server address, default `127.0.0.1:8080`
- -i (REPORT_INTERVAL) - interval for sending metrics to the server, default 10s
- -p (POLL_INTERVAL) - update metrics interval, default 2s
